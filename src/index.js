import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import App from './App';

window.apiurl = "http://www.fabriciobiron.me"; //Development
// window.apiurl = "";  // Production

ReactDOM.render(<App />, document.getElementById('root'));

 