import React, { Component } from 'react';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PubSub from 'pubsub-js'
// import { Transition } from 'react-transition-group'


export default class MainDetail extends Component{
    
    constructor(props)
    {
        super(props)
        this.state = {
            showDetail: this.props.show,
            positioningSize: 400,
            positioningValue: 0
        } 

    }

    componentDidMount(){
        
        let _this = this
        PubSub.subscribe('changeContent',(item,value)=>{
            _this.setState({positioningValue: -(this.state.positioningSize * value.key) })
        })

    }

    controlContentVisibitily()
    {
        return ( this.props.show) ? 'main-detail' : 'main-detail hidden'        
    }

    contentItem(){
        return  <div className="content"  dangerouslySetInnerHTML={{ __html: this.props.content}}></div>  
    }


    render(){
        return(
            <div className={this.controlContentVisibitily()}>
                <a className="close_button" onClick={this.props.closeDetail.bind(this)}>   
                    <i className="far fa-times-circle fa-2x"></i>
                </a>
                <div className="content-wrapper" style={{top: this.state.positioningValue}}>
                {
                    this.props.content.map((v)=>{
                        return <div key={v.id} className="content-item"> <div className="content-text"  dangerouslySetInnerHTML={{ __html: v.content.rendered}}></div> </div>
                    })
                }
                </div>
            </div>
        )
    }

}