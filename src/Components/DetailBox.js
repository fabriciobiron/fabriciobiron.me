import React, { Component } from 'react';
import PubSub from 'pubsub-js'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class Content extends Component{

    constructor(props){
        super(props)
        this.state = {
        }
    }

    closeDetail(){
        PubSub.publish('showDetail');
    }

    render(){

        return(

            <div key="2" class="container">

                <a key="1" className="close_button" onClick={this.closeDetail}>   
                    <i class="far fa-times-circle fa-2x"></i>
                </a>
                <h2>{this.props.title}</h2>
                <div className="text" dangerouslySetInnerHTML={{ __html: this.props.text }}></div>
               
            </div>
        )
    }

}

export default class DetailBox extends Component {
    constructor(props){
        super(props)
        this.state = {
            style: "detail",
            post: {},
            showContent: false
        }
    }

    componentDidMount(){
        PubSub.subscribe('showDetail',(emitter,value)=>{

            if(value){
                this.setState({style: "detail expanded"})
                this.setState({showContent: true})
                this.getContent(value)
            }else{
                this.setState({style: "detail"})
                 this.setState({showContent: false})
            }
        })
    }

    getContent(id){
        let _this = this 

        fetch(window.apiurl+'/wp-json/wp/v2/posts/'+id).then((response) => {
            return response.json()
        }).then((data)=>{
            _this.setState({post: data})
        })
    }



    controlShowContent(){
        if(this.state.showContent){
            return <Content title={this.state.post.title && this.state.post.title.rendered} text={this.state.post.content && this.state.post.content.rendered}/>

        }else{
           return null
        }
                    
    }


    render(){

        return(

            <div className={this.state.style }>
             
     
                <ReactCSSTransitionGroup
                    transitionName="example"
                    transitionEnterTimeout={2000}
                    transitionLeaveTimeout={2800}>
  
                    { this.controlShowContent()}
                
                </ReactCSSTransitionGroup>
                <a name="detail" ></a>
            </div>

        )
    }
}