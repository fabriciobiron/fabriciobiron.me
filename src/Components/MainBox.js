import React, { Component } from 'react';
import PubSub from 'pubsub-js'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import MainDetail from './MainDetail';


class Page extends Component{

    constructor(props)
    {
        super(props)   
        this.state = {
            showDetail: false
        }
    }

    // showContentFunction()
    // {
    //     // let toggle = !this.state.showDetail
    //     // this.setState({showDetail: toggle})
    // }

    
    controlType(type)
    {
        if(type === 'page'){
            return <h3 onClick={this.props.showContentFunction.bind(this)}>{this.props.title}</h3>
        }else{
            return <h2 onClick={this.props.showContentFunction.bind(this)}>{this.props.title}</h2>
        }
    }



    render()
    {
        return (
            <dt className="page-menu-item">
                {this.controlType(this.props.type)}
            </dt>
        )
    }
}



export default class MainBox extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            pages:[],
            detailContent: '',
            showDetail: false
        } 
    }

    getContent()
    {
        let _this = this 
        fetch(window.apiurl+'/wp-json/wp/v2/pages').then((response)=>response.json()).then((data)=>{
            _this.setState({pages:data})
        })
    }

    componentDidMount()
    {
        this.getContent();

    }

    controlMainDetail(key){
        this.setState({showDetail: true})

        PubSub.publish('changeContent',{key: key})
    }

    closeDetailCall(){
        this.setState({showDetail: false})
    }
    
    render()
    {
        return(
            <div class="main">
                <MainDetail  show={this.state.showDetail} content={this.state.pages} closeDetail={this.closeDetailCall.bind(this)} /> 
                <dl className="page-menu">
                    {
                        this.state.pages.map((v,k)=>{
                            if(v.slug === 'sample-page'){
                                return <Page showContentFunction={this.controlMainDetail.bind(this,k)} type="about" title={v.title.rendered} content={v.content.rendered} />
                            }else{
                                return <Page showContentFunction={this.controlMainDetail.bind(this,k)} type="page" title={v.title.rendered} content={v.content.rendered} />
                            }
                        })
                    }

                    <dt className="profiles page-menu-item">
                        <a href="http://github.com/fabriciobiron" target="_blank">
                            <i class="fab fa-git-square fa-3x"></i>
                        </a>
                        <a href="https://www.linkedin.com/in/fabriciobiron-ca"  target="_blank">
                            <i class="fab fa-linkedin fa-3x"></i>
                        </a>
                    </dt>
                </dl>

               

            </div>
        )
    }
}