import React, { Component } from 'react';
import PubSub from 'pubsub-js'
import axios from 'axios'

class ListItem extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <li ><a href="#detail" className="link" onClick={this.props.onClick.bind(this)}>{this.props.title}</a></li>
        )
    }
} 

export default class ContentBox extends Component {

    constructor(props){
        super(props)
        this.state = {
            posts: []
        }
        this.fuck = []
    }

    getPosts(){
        let _this = this 
        fetch(window.apiurl+'/wp-json/wp/v2/posts').then((response) => {
            return response.json()
        }).then((data)=>{

            _this.setState({posts: data})
            
        })
    }
    

    componentDidMount(){
    
        this.getPosts();
    }

    callDetail(id){
        PubSub.publish('showDetail',id)
    }

    render(){
        return(
            <div className="list">

                <div className="container">

                    <h6>Playground</h6>
                    
                    <ul>
                        {
                            this.state.posts.map((v,k)=>{
                                if(k === 0){
                                    return <h3 className='link' key={v.id} onClick={this.callDetail.bind(this,v.id)}><a href="#detail">{v.title.rendered}</a></h3>
                                }else{
                                    return <ListItem title={v.title.rendered} key={v.id} onClick={this.callDetail.bind(this,v.id)} />
                                }
                            })
                        }
                    </ul>

                </div>

            </div>
        )
    }
}