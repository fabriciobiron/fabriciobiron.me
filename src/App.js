import React, { Component } from 'react';
import MainBox from './Components/MainBox';
import ContentBox from './Components/ContentBox';
import DetailBox from './Components/DetailBox';
import './assets/css/milligram.css';



class App extends Component {
  render() {
    return (
      <div className="App">
        <div class="site">
            
            <MainBox/>
            <ContentBox/>
            <DetailBox />
            
        </div>{/* end of .site*/}
      </div>

    );
  }
}

export default App;
